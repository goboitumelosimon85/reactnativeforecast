import React, { Component } from 'react';
import {View, StyleSheet, Button, Platform, Linking, PermissionsAndroid, Text, FlatList, ImageBackground} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import axios from 'axios';
import Forecast from './components/forecastList';
import {key} from '../app.json';


export default class App extends Component {

    // otherDayObject:{
    //     date:'',
    //     current:'',
    //     description:'',
    //     day:''
    // }

    componentDidMount(){
        this.getCoordinates();
    }

    state = {
        currentDate: '',
        current:'0',
        min:'0',
        max:'0',
        description:'Clear',
        lat:'',
        long:'',
        otherDays:[],
        display:''
    }

    openApp(){
        let scheme = Platform.OS === 'ios' ? 'maps:' : 'geo:';
        let url = scheme + `${this.state.lat},${this.state.long}`;
        this.openExternalApp(url);
    }

    openExternalApp = (url) => {
        Linking.canOpenURL(url).then(supported => {
          if (supported) {
            Linking.openURL(url);
          } else {
            alert('Unable to open app');
          }
        });
    }

    async getCoordinates() {

        let hasLocationPermission = await PermissionsAndroid.request( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

  
        if (hasLocationPermission) {
            Geolocation.getCurrentPosition(
                (position) => {

                    this.getDate();

                    const lat = position.coords.latitude;
                    const long = position.coords.longitude;

                    this.setState(prevState=>{
                        return({
                            lat: lat,
                            long: long
                        })
                    })

                    this.clear();
                    this.getCurrentWeather(lat,long);
                    this.getForecastWeather(lat,long);
                },
                (error) => {
                    // See error code charts below.
                    console.log(error.code, error.message);
                },
                { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
            );
        }
        else{
            console.log('Permissions not granted');
        }
    }

    clear(){
        this.setState(prevState=>{
            return({
                //current:'',
                min:'',
                max:'',
                description:'',
                lat:'',
                long:'',
                otherDays:[]
            })
        })
    }

    getCurrentWeather(lat,long){

        const endpoint = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${key}&units=metric`;

        axios.get(endpoint)
        .then(res=>{
            let weather = res.data.weather[0];
            let temperature = res.data.main;

            let min = Math.floor(temperature.temp_min);
            let current = Math.floor(temperature.temp);
            let max = Math.floor(temperature.temp_max);
            let description = weather.main;

            let display = this.getDisplayName(description);

            this.setState(prevState=>{
                return({
                    min : min,
                    current : current,
                    max : max,
                    description : description,
                    display:display
                })
            })
        })
        .catch(err=>{
            console.log(err);
        })
    }

    getDisplayName(description){
        
        if(description == 'Clear'){
            tmp = 'Clear';
        }
        else if(description == 'Rain'){
            tmp = 'Rainy';
        }
        else{
            tmp = 'Cloudy';
        }

        return tmp;
    }

    getDate(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = `${yyyy}-${mm}-${dd}`

        this.setState(prev=>{
            return({
                currentDate: today
            })
        })
    }

    getForecastWeather(lat,long){
       
        const endpont = `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${long}&appid=${key}&units=metric`;

        //console.log(endpont);

        axios.get(endpont)
        .then(res=>{
            //Retrieve temperature list
            let list = res.data.list;

            let arrDays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

            list.forEach(item=>{
                //Check if object has been added yet
                let date = item.dt_txt.substring(0,10);

                if(date != this.state.currentDate){
                    if(this.checkItem(date)){

                        let RealDate = new Date(date);
                        let index = RealDate.getDay()
                        //Create weather object
                        let otherDayObject = {
                            id: new Date(),
                            date: date,
                            current: Math.floor(item.main.temp),
                            description: item.weather[0].main,
                            day: arrDays[index]
                        }

                        this.setState(prevState=>{
                            return({
                                otherDays: [...prevState.otherDays, otherDayObject]
                            })
                        })
                    }
                }
            });

            console.log(this.state.otherDays);
        })
        .catch(err=>{
            console.log(err);
        })
    }

    checkItem(date){
        let status = true;

        if(this.state.otherDays.length == 0){
            return status;
        }
        else{

            let existingObject = this.state.otherDays.find(tmp=> tmp.date == date);

        
            if(existingObject == null || existingObject == undefined){
                return status;
            }
            else{
                status = false;
                return status;
            }
        }
    }

    render(){
        //this.getDate();
        const {container,topContainer,topContainerFooter,topContainerHeader, header,footerContent, forecastContainer, footerHeader,bottom,footerDescription} = styles;

        let path = require('./Assets/Images/sea_sunnypng.png');
        if(this.state.description == 'Clear'){
            path = require('./Assets/Images/sea_sunnypng.png');
        }
        else if(this.state.description == 'Rain'){
            path = require('./Assets/Images/sea_rainy.png');
        }
        else{
            path = require('./Assets/Images/sea_cloudy.png');
        }

        return(
            <View style={container}>
                
                <View style={topContainer}>
                    <View style={{flex:1}}>
                        <ImageBackground style={{width: '100%', height:'100%'}} source={path}>
                            <View style={topContainerHeader}>
                                <Text style={header}>{this.state.current}{'\u00b0'}</Text>
                                <Text style={header}>{this.state.display}</Text>
                            </View>   
                        </ImageBackground>
                    </View>
                    

                    <View style={[bottom, this.state.description == 'Clear' ? styles.sunny:'', this.state.description == 'Rain' ? styles.rainy: '', this.state.description == 'Clouds' ? styles.cloudy: '']}>
                        <View style={topContainerFooter}>
                            <View style={footerContent}>
                                <Text style={footerHeader}>{this.state.min}{'\u00b0'}</Text>
                                <Text style={footerDescription}>min</Text>
                            </View>

                            <View style={footerContent}>
                                <Text style={footerHeader}>{this.state.current}{'\u00b0'}</Text>
                                <Text style={footerDescription}>Current</Text>
                            </View>

                            <View style={footerContent}>
                                <Text style={footerHeader}>{this.state.max}{'\u00b0'}</Text>
                                <Text style={footerDescription}>max</Text>
                            </View>
                        </View>
                    </View>
                </View>
                
                <View style={[forecastContainer, this.state.description == 'Clear' ? styles.sunny:'', this.state.description == 'Rain' ? styles.rainy:'', this.state.description == 'Clouds' ? styles.cloudy: '']}>
                    <FlatList
                        data={this.state.otherDays}
                        renderItem={ ({item}) =>(
                            <Forecast day={item.day} temperature={item.current} description={item.description}/>
                        )}

                        keyExtractor={item=>item.id+String(Math.random())}
                    />
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        
    },
    sunny:{
        backgroundColor: "#47AB2F"
    },
    cloudy:{
        backgroundColor: "#54717A"
    },
    rainy:{
        backgroundColor: "#57575D"
    },
    forecastContainer:{
        flex:1,
        paddingTop: 10
    },
    header:{
        fontSize:20,
        fontWeight: "bold",
        color: "white"
    },
    footerHeader:{
        fontSize:18,
        fontWeight: "bold",
        color: "white"
    },
    footerDescription:{
        fontSize:18,
        color: "white"
    },
    topContainer:{
        flex:1,
        
    },
    topContainerHeader:{
        flex:1,
        alignItems: "center",
        marginTop: 20
    },
    topContainerFooter:{
        flex:1,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft:10,
        paddingRight:10,
        
    },
    footerContent:{
        alignItems: "center"
    },
    bottom:{
        justifyContent: "flex-end", 
        height:60,
        borderBottomColor: "white",
        borderBottomWidth: 1
    }
})