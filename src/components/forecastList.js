import React, { Component } from 'react';
import { View, StyleSheet, Text, Image } from "react-native";

export default class ForecastList extends Component{

    getImage(){
        let path = require('../Assets/Icons/clear.png');
        if(this.props.description == 'Clear'){
            path = require('../Assets/Icons/clear.png');
        }
        else if(this.props.description == 'Rain'){
            path = require('../Assets/Icons/rain.png');
        }
        else{
            path = require('../Assets/Icons/partlysunny.png');
        }

        return <Image source={path} style={styles.image}/>
    }
    render(){
        const {container, header, dayImg} = styles;
        return(
            <View style={container}>
                <View style={dayImg}> 
                    <Text style={header}>{this.props.day}</Text>
                    {this.getImage()}
                </View>
                <Text style={header}>{this.props.temperature}{'\u00b0'}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    image:{
        width:20,
        height:20
    },
    dayImg:{
        flexDirection:"row",
        justifyContent:"space-between",
        width:'50%'
    },
    container:{
        flex:1,
        flexDirection:"row",
        justifyContent: "space-between",
        marginLeft: 20,
        marginRight:20
    },
    header:{
        fontSize: 18,
        fontWeight: "bold",
        color: "white"
    }
})